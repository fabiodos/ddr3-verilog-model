onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb/sdramddr3_0/rst_n
add wave -noupdate /tb/sdramddr3_0/ck
add wave -noupdate /tb/sdramddr3_0/ck_n
add wave -noupdate /tb/sdramddr3_0/cke
add wave -noupdate /tb/sdramddr3_0/cs_n
add wave -noupdate /tb/sdramddr3_0/ras_n
add wave -noupdate /tb/sdramddr3_0/cas_n
add wave -noupdate /tb/sdramddr3_0/we_n
add wave -noupdate /tb/sdramddr3_0/ba
add wave -noupdate /tb/sdramddr3_0/addr
add wave -noupdate /tb/sdramddr3_0/odt
add wave -noupdate /tb/sdramddr3_0/dm_tdqs
add wave -noupdate -color Yellow /tb/sdramddr3_0/dq
add wave -noupdate /tb/sdramddr3_0/dqs
add wave -noupdate /tb/sdramddr3_0/dqs_n
add wave -noupdate /tb/sdramddr3_0/memory_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {382032 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {365189 ps} {391781 ps}
