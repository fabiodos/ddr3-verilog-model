vlog -64 -incr -mfcu -sv -work work  "+incdir+" "+define+den4096Mb+define+sg187E+define+x16" ddr3.v
vlog -reportprogress 300 -64 -incr -mfcu -sv -work work "+incdir+" "+define+den4096Mb+define+sg187E+define+x16" tb.v
vopt work.tb +acc +debugdb -o tb_opt
vsim -debugDB work.tb_opt
add log -r sim:/tb/sdramddr3_0/*
do wave.do

run 205 us
